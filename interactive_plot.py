#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd


# In[2]:


file_path = r"debug1.csv"
df0 = pd.read_csv(file_path, sep=';',nrows=2, header=None, encoding="cp1251",decimal=",")
df0


# In[3]:


StartTime = df0.loc[0,1]

StartTime


# In[4]:


# Путь к файлу CSV
file_path = r"debug1.csv"
df1 = pd.read_csv(file_path, skiprows=2, sep=';',decimal=",", encoding="cp1251")


# In[5]:


df1['x'] = df1['время'] - StartTime
df1['y'] = df1['положение']


# In[6]:


df1[['x','y']]


# In[7]:


file_path = r"debug2.csv"
df2 = pd.read_csv(file_path, sep=';', encoding="cp1251",decimal=",")

# Вывод DataFrame
df2


# In[8]:


df2['xp1'] = df2['t1'] - StartTime


# In[9]:


df2['xp2'] = df2['t2'] - StartTime
df2['xp3'] = df2['t3'] - StartTime


# In[10]:


import pandas as pd
import plotly.graph_objs as go
from plotly.subplots import make_subplots


# In[14]:


# Создаем объект для размещения графиков
fig = make_subplots(rows=1, cols=1)

# Добавляем интерактивный график
scatter = go.Scatter(x=df1.x, y=df1.y, mode='lines+markers', name='График')
fig.add_trace(scatter)

scatter = go.Scatter(x=df2['xp1'], y=df2['vp1'], mode='markers', name='нач')
fig.add_trace(scatter)

scatter = go.Scatter(x=df2['xp2'], y=df2['vp2'], mode='markers', name='пик')
fig.add_trace(scatter)

scatter = go.Scatter(x=df2['xp3'], y=df2['vp3'], mode='markers', name='кон')
fig.add_trace(scatter)

scatter = go.Scatter(x=df2['xp2'], y=df2['ampf1'], mode='lines+markers', name='амплТуда', yaxis='y2')
fig.add_trace(scatter)

scatter = go.Scatter(x=df2['xp3'], y=-df2['ampf2'], mode='lines+markers', name='амплОбратно', yaxis='y2')
fig.add_trace(scatter)



# scatter = go.Scatter(x=df3.index, y=df3['min'], mode='markers', name='min')
# fig.add_trace(scatter)

# scatter = go.Scatter(x=newmax, y=[ vals[x] for x in newmax  ], mode='markers', name='newmax' )
# fig.add_trace(scatter)
# scatter = go.Scatter(x=newmins, y=[ vals[x] for x in newmins  ], mode='markers', name='newmin')
# fig.add_trace(scatter)

# scatter = go.Scatter(x=df3.index, y=df3['min'], mode='markers', name='min')
# fig.add_trace(scatter)


# Устанавливаем заголовок
#fig.update_layout(title='Найденные мин-макс')
fig.update_layout(yaxis2=dict(overlaying='y', side='right',title='перемещение метры'))
# Добавляем подпись для левой шкалы оси y (градусы)
fig.update_yaxes(title_text='перемещение градусы', secondary_y=False)

# Размещаем легенду снизу
fig.update_layout(legend=dict(orientation='h', yanchor='bottom', y=1.02, xanchor='right', x=1))


# Отображаем график в оффлайн-режиме
from plotly.offline import plot
plot(fig, filename='interactive_plot.html')

