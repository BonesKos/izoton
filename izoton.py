#!/usr/bin/env python
# coding: utf-8

# In[1]:


# -*- coding: utf-8 -*-
from PyQt5 import QtWidgets, uic
from PyQt5.QtGui import QIcon, QPixmap, QIntValidator, QDoubleValidator 
from PyQt5.QtCore import Qt, pyqtSignal, QObject, QTimer, QSignalMapper
from PyQt5 import  QtNetwork
import sys 
from statistics import mean, stdev
from time import localtime ,time as gettime, strftime
import json
import csv
#import numpy as np
from scipy.signal import find_peaks


# In[2]:


app = QtWidgets.QApplication([])


# # Программа для тернажеров Изотон
# Программно-измерительные средств для работы тренажеров “Изотон”, позволяющие на основе измерения угла и ввода его в ПК рассчитывать кинематические данные и нагрузку.
# - Для начала работы необходимо подключить датчик и обеспечить доступ через TCP-порт. Для win10 предлагаем использовать hub4com.exe. Выбор COM-порта происходит через параметры при запуске: serial2tcp.bat
# - В настройках текущего ПО выбрать необходимый порт, направлние движения и значение нечуствительности угла.

# ## Доступ к данным

# ### Получение конфигурации

# In[3]:


class Cconf(QObject):
    signalConfChanged = pyqtSignal()
    _default = [
    ['dt',"19.03.2023",'Дата'],
    ['userId',123,'Личный №'],
    ['devId',1,'Тренажер №'],
    ['devName',"Нижняя тяга",'Наименование'],
    ['force1',13.0,'Усилие рычаг 1'],
    ['force4',36.0,'Усилие рычаг 4'],
    ['force7',56.0,'Усилие рычаг 7'],
    ['force10',82.0,'Усилие рычаг 10'],
    ['k1',1.0,'К1 – градуировка'],
    ['k2',1.0,'К2 – нелинейность'],
    ['k3',1.0,'К3 – динамика'],
    ['k4',0.0,'К4 – масса < 75 кг'],
    ['k5',0.0,'К5 – масса > 75 кг'],
    ['k6',1.0,'К6 – обратно'],
    ['maxAngle',33,'Максим. угол'],
    ['kay',[0.0401,0.0402,0.0403,0.0404,0.0405,0.0406,0.0407,0.0408,0.0409,0.0410],'КАУ 1..10 – амплитуда'],
    ['calcPull',0,'Расчетное усилие, %'],
    ['conn','127.0.0.1:5001','ip:порт датчика'],
    ['angle','НЕТ СВЯЗИ С ДАТЧИКОМ !!!','Датчик'],
    ['f1f2','+','F1 (туда), угол "+"|"-"'],
    ['delta',2.0,'Нечувств. угол'],
    ['dt2',"05.02.2023",'Дата'],
    ['userId2',123,'Личный №'],
    ['training',0,'Тренировка'],
    ['podhod',1,'Подход №'],
    ['level',4,'№ на рычаге'],
    ['calcPull2',0,'Значение усилия'],
    ['weight',75,'Масса человека'],
    ['time',300,'Время теста'],
    ['fullScreen',0,'1=полный экран'],
    ]
    conf={}
    f1 = 0.0 #Номинальное усилие F1 (преодолевающий режим работы, “туда”).
    f2 = 0.0 #Номинальное усилие F2 (уступающий режим работы, “обратно”).

    def __init__(self):
        #super(Cconf, self).__init__()
        QObject.__init__(self)

        try:
            with open("conf.json", "r", encoding='utf8') as my_file:
                self.conf = json.loads(my_file.read())
        except:
              print("ERR conf.json") 

        for i,df in enumerate(self._default):
            if (df[0] not in self.conf) or  ('val' not in self.conf[df[0]] ) or ('name' not in self.conf[df[0]] ) :
                self.conf[df[0]]={'val':df[1],'name':df[2]}
                print(df[0])

    def saveConf(self):


        M = self.conf['weight']['val']
        L = self.conf['level']['val']
        
        K1 =  self.conf['k1']['val']
        K2 =  self.conf['k2']['val']
        K3 =  self.conf['k3']['val']
        K4 =  self.conf['k4']['val']
        K5 =  self.conf['k5']['val']
        K6 =  self.conf['k6']['val']
        
        naklon = 1.0
        otrezok = 0.0
        if L <=4 :
            #global naklon, otrezok
            naklon =  (self.conf['force4']['val'] - self.conf['force1']['val'])/(4-1)
            otrezok =  self.conf['force1']['val'] - naklon*1
        elif L <=7 :
            naklon =  (self.conf['force7']['val'] - self.conf['force4']['val'])/(7-4)
            otrezok =  self.conf['force4']['val'] - naklon*4
        elif L <=10 :
            naklon =  (self.conf['force10']['val'] - self.conf['force7']['val'])/(10-7)
            otrezok =  self.conf['force7']['val'] - naklon*7
        
        preF=(self.conf['level']['val'] * naklon + otrezok)
        
        self.conf['calcPull2']['val']=  preF*K1*K2*K3*(1+K4*(M-75)/M) if M<75 else preF*K1*K2*K3*(1+K5*(M-75)/M)         
        self.conf['calcPull']['val']= self.conf['calcPull2']['val']
        self.f1 = self.conf['calcPull2']['val'] * 0.01* M 
        self.f2 = self.f1 * K6

        with open('conf.json', 'w', encoding='utf8') as f:
            json.dump(self.conf, f, ensure_ascii=False, indent=1)
        with open('conf2.json', 'w', encoding='utf8') as f:
            json.dump(self.conf, f, ensure_ascii=False, indent=1)
        with open('conf3.json', 'w', encoding='utf8') as f:
            json.dump(self.conf, f, ensure_ascii=False, indent=1)
        self.signalConfChanged.emit()
        


# In[ ]:





# In[4]:


#print(naklon,otrezok, c.conf['level']['val'], c.conf['level']['val']*naklon + otrezok)


# In[5]:


c = Cconf()
c.saveConf()


# ### Подключение к датчику

# In[6]:


class Cdata(QObject):
    """Класс доступа к датчику"""
    sensorConnected = False
    signal_sensorConnected = pyqtSignal()
    signal_sensorsStats = pyqtSignal(int,float,float,float,float,int)
    """Сигнал статистики Гц, срЗнач, Откл,срЗнач, Откл"""
    timerStat =   QTimer()
    
    signal_sensorDisconnected = pyqtSignal()
    signal_sensorVal = pyqtSignal(float,float)
    remoteAddress, port = '127.0.0.1',5001
    sockReadStr = ''
    
    socket = None
    timer =   QTimer()
    clock = 0
    
    hz =0 
    lastStd1 = 0
    lastStd2 = 0
    lastMeanVal1 = 0.0
    lastMeanVal2 = 0.0
    errorCnt = 0
    _vals1 = []
    _vals2 = []
    
    def __onDisconnected(self):
        self.sensorConnected = False
        self.signal_sensorDisconnected.emit()

    def timeout(self):
        """слот переподключения к датчику каждые 2сек"""
        if not self.sensorConnected:
            self.clock += 1

            self.socket = QtNetwork.QTcpSocket()   
            self.socket.connectToHost(self.remoteAddress, self.port)
            if self.socket.waitForConnected(1000) : 
                self.socket.setSocketOption(QtNetwork.QAbstractSocket.KeepAliveOption, 1)
                self.socket.setSocketOption(QtNetwork.QAbstractSocket.LowDelayOption, 1)
                self.socket.disconnected.connect(self.__onDisconnected) 
                self.socket.readyRead.connect(self.sockReadyRead)
                self.sensorConnected = True
                self.signal_sensorConnected.emit()

        
        
    def timeoutTimerStat(self):
        """Подсчет статистики за 0.5 сек"""
        self.hz = len(self._vals1)*2
        

        if self.hz >= 4 : 
            self.lastMeanVal1 = mean(self._vals1)
            self.lastMeanVal2 = mean(self._vals2)
            self.lastStd1 = stdev(self._vals1)*100.0/25.0
            self.lastStd2 = stdev(self._vals2)*100.0/25.0
        self.signal_sensorsStats.emit(self.hz,self.lastMeanVal1,self.lastStd1,self.lastMeanVal2,self.lastStd2,self.errorCnt)
        self._vals1 = []
        self._vals2 = []

    
    def sockReadyRead(self):
        """Обработка полученных данных"""
        ba = self.socket.readAll()
        #self.signal_sensorVal.emit(len(ba))
        #if len(self.sockReadStr) > 12 : self.sockReadStr = ''
        try:
            self.sockReadStr += ba.data().decode()
            if len(self.sockReadStr) >=10 and self.sockReadStr.__contains__('\r\n'):
                valsStr = self.sockReadStr.split('\r\n')[-2]

                if len(valsStr) > 0 : 
                    self.sensorVal1 = float(valsStr.split(',')[0])
                    self.sensorVal2 = float(valsStr.split(',')[1])
                    self._vals1.append(self.sensorVal1)
                    self._vals2.append(self.sensorVal2)
                self.sockReadStr = self.sockReadStr.split('\r\n')[-1]
                self.signal_sensorVal.emit(self.sensorVal1,self.sensorVal2)

        except:
            #print('ERR')
            self.sockReadStr = ''
            self.errorCnt += 1
                
    
    def __init__(self):
        QObject.__init__(self)
        
        self.remoteAddress = c.conf['conn']['val'].split(':')[0]
        self.port = int(c.conf['conn']['val'].split(':')[1]) 
        
        self.timer.timeout.connect(self.timeout)
        self.timer.start(2000)
        
        self.timerStat.timeout.connect(self.timeoutTimerStat)
        self.timerStat.start(500)


# In[7]:


d = Cdata()


# ## Обработка логики алгоритмов расчета

# In[8]:


class Clogic(QObject):
    """Класс реализующий алгоритм расчета работы, мощности и прочего"""
    signalCalcRes = pyqtSignal(list)
    signalMoveRes = pyqtSignal(list)
    """сигнал готовности расчитанных данных"""
    signalTestEnd = pyqtSignal(QObject)
    """сигнал завершения расчетов по истечению времени"""
    timer =   QTimer()
    _logicState = 'stop'
    _val = 0.0
    _valf1 = 0.0
    _startt = [] #Массив стартов
    _stopt = [] #Массив остановов

    _lvt = [] #Массив временных меток
    _lvals1 = [] #Массив от датчика 1
    _lvals2 = [] #Массив от датчика 2
    _lvals = [] #Массив суммарного сигнала от позиции старта
    
    _valf2 = 0.0
    _posDeltaAngle = c.conf['delta']['val']
    _posCur = 0.0
    _posPrev = 0.0
    _posStart1 = 0.0
    _posStart2 = 0.0
    
    _posStart = 0.0
    _isTurnStartPos = False
    states = ['zonaStart', 'f1', 'f2',]
    time = 0.0
    move = 0.0
    work = 0.0
    power= 0.0
    _workPreTime = gettime()
    _workPre = 0.0
    _amp = 0.0
    _deltaPos = 0
    _deltaPosPre = 0
    _isF1 = True
    _isF1pre = True
    _isStartZone = False

    _moveStartTime = gettime()
    _powerTime = gettime()
    _startTime = gettime()
    
    debuglf = ['_valf1', '_valf2', 'time', '_val', '_posDeltaAngle',  'move', 'work', 'power', '_posCur', '_posPrev', '_posStart', '_posStart1', '_posStart2',  '_logicState', '_preDeltaPos', '_deltaPos', '_startTime', '_moveState', '_moveStartTime', '_moveStartWork',  'debugState', '_isStartZone', '_amp', '_powerTime']


  
    debugState = 0
    """В режиме отладки (2) пишем данные в csv-файл"""
    
    def __init__(self):
        QObject.__init__(self)
        self.timer.start(200)
        self.timer.timeout.connect(self._signalCalcResTimer)
    
    def _signalCalcResTimer(self):
        """Расчет времени событие по обновлению экрана"""
        if self._logicState == 'run' : 
            self.time += 0.2
   
        self.signalCalcRes.emit([self.time])
        
        
 
        
        if self.time >= c.conf['time']['val'] and self._logicState != 'stop': 
            self._logicState == 'stop'
            self.signalTestEnd.emit(self)
    
    def slotDebugStateChanged(self, state):
        """При включении отладки пересоздаем файл лога данных"""
        self.debugState = state
        if self.debugState == 2:
            with open('degug.csv', 'w', newline='\n') as csvfile:
                csvfile.write(";".join(c.conf.keys()) + "\n")
                csvfile.write(";".join([ str(c.conf[k]['val']) for k in c.conf] )  + "\n")
                csvfile.write(";".join(self.debuglf)  + "\n")
                
            
        
        
    def find_peaksWork(self):
        """Поиск всех движений (пиков) и расчет работы и мощности на найденных пиках"""
        


        kay = c.conf['kay']['val'][c.conf['level']['val']-1]
        #kay = 0.04
        prominence= 0.15 / kay # пересчет 15см в угол для фильтра
        #16*kay, kay, prominence
        fullwork = 0.0
        moveCnt = 0
        debugline =[]
        minmaxs=[]
        err = 0
        pwr = 0
        for j in range(len(self._lvals)):
            vals = self._lvals[j]
            peaks,_ = find_peaks(vals, prominence=prominence,  distance=15)
            peaks2,_ = find_peaks([-v for v in vals], prominence=prominence,  distance=15)


            immax = list(peaks)
            immin = list(peaks2)

            #Убираем двойные точки в минимумах и добавляем точки конца и начала интервала
            newmins = [0]
            for i in range(len(immax) -1):
                minss  = [ind for ind in immin if immax[i] <= ind <= immax[i+1]]
                if(len(minss)==0): continue
                elif(len(minss)==1): newmins.append(minss[0])
                else:
                    mvvls = [vals[ind] for ind in minss]
                    norm = minss[mvvls.index(min(mvvls))]
                    newmins.append(norm)
            newmins.append(len(vals)-1)
            #immin , newmins

            #Убираем двойные точки в максимумах
            newmax = []
            for i in range(len(newmins) -1):
                maxss  = [ind for ind in immax if newmins[i] <= ind <= newmins[i+1]]
                #print(maxss)
                if(len(maxss)==0): print('ERROR')
                elif(len(maxss)==1): newmax.append(maxss[0])
                else:
                    mvvls = [vals[ind] for ind in maxss]
                    norm = maxss[mvvls.index(max(mvvls))]
                    newmax.append(norm)
            newmax
            


            #считаем работу

            moveCnt += len(newmax)
            minmaxs.append([j,moveCnt,newmins,newmax])
            if (len(newmins)  != len(newmax) + 1 ):
                err+= 1
                print(f"errorCount {err}")
                continue
            for i in range(len(newmax)):
                i1= newmins[i]
                i2 = newmax[i]
                i3 = newmins[i+1]
                if ~(i1 < i2 < i3): 
                    err += 1000
                    print(f"errorCount {err}")

                vp1 = vals[i1]
                vp2 = vals[i2]
                vp3 = vals[i3]


                ampf1 = abs(vp2-vp1)*kay
                ampf2 = abs(vp3-vp2)*kay
                #vp2-vp1 , ampf1,ampf2

                workf1 = ampf1*c.f1*9.807
                workf2 = ampf2*c.f2*9.807
                fullwork += workf1+workf2
                dt = self._lvt[j][i3] - self._lvt[j][i1]
                pwr = (workf1+workf2)/dt if (dt!=0) else 0
                debugline.append([j,i,i1,i2,i3,self._lvt[j][i1],self._lvt[j][i2],self._lvt[j][i3],vp1,vp2,vp3,c.conf['level']['val'],kay,ampf1,ampf2,c.f1,c.f2,workf1,workf2,dt,pwr,fullwork])
        
        return moveCnt,pwr,fullwork, minmaxs, debugline, "пауза,i,i1,i2,i3,t1,t2,t3,vp1,vp2,vp3,level,kay,ampf1,ampf2,c.f1,c.f2,workf1,workf2,dt,pwr,fullwork".replace(',',';')
        
 
    def slotVal(self, valf1,valf2):
        """Слот обработки данных от датчика"""
       
        self._valf1 = valf1
        self._valf2 = valf2

        
        if self._logicState != 'run': return

    
        self._val = (valf1 -  self._posStart1) + (valf2 -  self._posStart2)
        
        val = self._val
                
        self._lvt[len(self._startt)-1].append(gettime())
        self._lvals1[len(self._startt)-1].append(valf1)
        self._lvals2[len(self._startt)-1].append(valf2)
        self._lvals[len(self._startt)-1].append(val)
        
        if abs(val - self._posCur) >= self._posDeltaAngle: 
            move = False
            self._posPrev = self._posCur
            self._posCur = val
            
            if c.conf['f1f2']['val'] == '+':
                self._isStartZone =  ( (self._posCur - self._posStart) <= self._posDeltaAngle )  
            else :
                self._isStartZone = ( (self._posStart - self._posCur) <= self._posDeltaAngle )
        
            self._preDeltaPos = self._deltaPos
            
            self._deltaPos = (self._posCur - self._posPrev)*(1 if c.conf['f1f2']['val'] == '+' else -1)
            self._amp = abs(self._deltaPos)* 0.024 #c.conf['kay']['val']
            
            if self._moveState == 'startZone' and self._deltaPos > 0:
                self._moveState = 'f1'
                self._moveStartTime = self._powerTime
                self._moveStartWork = self.work

         

            if self._moveState == 'f1' and self._preDeltaPos>0 and self._deltaPos < 0:
                self._moveState = 'f2'

            if  (self._moveState == 'f2') and self._isStartZone:
                '''Дошли до стартЗоны считаем движение''' 
                self._moveState = 'startZone'
                
                self.power = (self.work - self._moveStartWork)/(gettime()-self._moveStartTime)
                self._powerTime = gettime()
                self._moveStartTime = self._powerTime
                self._moveStartWork = self.work
                move = True

            if  (self._moveState == 'f2' and self._preDeltaPos <0 and self._deltaPos > 0)  :
                '''Разворот, возможно до или после старт зоны'''
                self._moveState = 'f1'
                self.power = (self.work - self._moveStartWork)/(gettime()-self._moveStartTime)
                self._moveStartTime = gettime()
                self._moveStartWork = self.work

                move = True
            
            if self._deltaPos >= 0 : #F1 если delta>0
                self.work += self._amp*c.f1*9.807
            else:
                self.work += self._amp*c.f2*9.807
   
            if move:
                move = False
                self.move += 1.0
                moveCnt,pwr,fullwork,_,_,_ = self.find_peaksWork()
#                 self.signalMoveRes.emit([self.time, self.move, self.work, self.power, self.powerMean()])
                self.signalMoveRes.emit([self.time, moveCnt, fullwork, pwr, fullwork/self.time if self.time > 0 else 0.0])
            


     
    
             
            

            
            #if self.debugState == 2:
            #    with open('degug.csv', 'a', newline='\n') as csvfile:
            #        csvfile.write(  ";".join(["{:.2f}".format(l.__dict__[lf]) if type(l.__dict__[lf]) == float else str(l.__dict__[lf]) for lf in l.debuglf ]).replace(".",",") + "\n" )
            

        
        return
        
            

        

    def start(self):
        """Старт теста"""
        self._posDeltaAngle = c.conf['delta']['val']
        self.time = 0.0
        self.move = 0.0
        self.work = 0.0
        self.power= 0.0
        self._posCur = self._val
        self._posPrev = self._val
        self._posStart = self._val
        
        self._posStart1 = d.lastMeanVal1
        self._posStart2 = d.lastMeanVal2
        
        self._logicState = 'run'
        self._preDeltaPos = 0 
        self._deltaPos = 0
        self._startTime = gettime()
        self._startt.append(self._startTime)
        self._lvt.append([])
        self._lvals1.append([])
        self._lvals2.append([])
        self._lvals.append([])
        
        self._moveState = 'startZone'
        self._moveStartTime = gettime()
        self._moveStartWork = self.work        
        return

    def pause(self):
        """Постановка на паузу"""
        self._logicState = 'pause'
        self._stopt.append(gettime())
        return
    
    def resume(self):
        """Продолжить тест"""
        self._posCur = self._val
        self._posPrev = self._val
        self._posStart = self._val
        
        self._posStart1 = d.lastMeanVal1
        self._posStart2 = d.lastMeanVal2
        
        self._logicState = 'run'
        self._moveStartTime = gettime()
        self._moveStartWork = self.work
        self._startt.append(gettime())
        self._lvt.append([])
        self._lvals1.append([])
        self._lvals2.append([])
        self._lvals.append([])
        return
    
    def stop(self):
        """Закончить тест"""
        self._logicState = 'stop'
        return
    
    def powerMean(self):
        """Расчет средней мощности за весь тест"""
        if self.time >0 :
            return self.work/self.time
        else :
            return 0.0


        
        


# ### Тестирование логики

# In[9]:


l = Clogic()


# In[10]:


# moveCnt,pwr,fullwork,_,_,_ = l.find_peaksWork()
# moveCnt,pwr,fullwork


# In[11]:


# l.slotVal(10)
# l.start()

# l.slotVal(13)
# l.slotVal(16)

# l.slotVal(16)
# l.slotVal(13)

# l.slotVal(10)
# l.__dict__


# ### Подключение данных к логике

# In[12]:


d.signal_sensorVal.connect(l.slotVal)


# ## Представление данных - интерфейс пользователя

# In[13]:


from izoton_ui import Ui_MainWindow  # импорт сгенерированного файла из ui

class window(QtWidgets.QMainWindow):
    """Класс основного окна"""

    pswdState = 1
    pswdTimer = QTimer()
    pswd135 = False

    
    lkay= ['kayLineEdit_0' + str(i+1) for i in range(9)] + ['kayLineEdit_10']
    """ Изменение КАУ на массив из 10 значений"""
    
    les2 = ['dt', 'userId', 'devId', 'devName', 'force1', 'force4', 'force7', 'force10', 'k1', 'k2', 'k3', 'k4', 'k5', 'k6',
            'maxAngle', 'kay',  'conn', 'f1f2','delta']
    """Перечень полей для настройки приложения"""
    
    inp = ['dt', 'userId', 'devId', 'devName', 'force1', 'force4', 'force7', 'force10', 'k1', 'k2', 'k3', 'k4', 'k5', 'k6', 'maxAngle', 'kay' ,'conn',   'userId2', 'training', 'podhod', 'weight', 'f1f2', 'delta','fullScreen']
    """Перечень полей для обработки ввода данных"""
    
    def _pswdTimeOut(self):
        """Таймаут ожидания следующей цифры для последовательности пароля"""
        self.pswdTimer.stop()
        self.pswdState = 1

    def refreshWins(self):
        """Метод заполнения полей из файла настроек"""
        self.ui.label_trName.setText('Тренажер №{} {}'.format(c.conf['devId']['val'],c.conf['devName']['val']))

        #global conf 
        for k in c.conf:
            self.ui.__dict__[k+'Label'].setText(c.conf[k]['name'])
            if k=='kay':
                for i,lk in enumerate(self.lkay):
                    self.ui.__dict__[lk].setText( str(c.conf[k]['val'][i]).replace('.',',') )
            else:
                self.ui.__dict__[k+'LineEdit'].setText( str(c.conf[k]['val']).replace('.',',') if  k not in ['conn','dt','dt2'] else  str(c.conf[k]['val']))
                
            self.update()
        k = 'userId2'
        self.ui.__dict__[k+'Label'].setText(c.conf['userId']['name'])
        self.ui.__dict__[k+'LineEdit'].setText(str(c.conf['userId']['val']))
        k = 'userId3'
        self.ui.__dict__[k+'Label'].setText(c.conf['userId']['name'])
        self.ui.__dict__[k+'LineEdit'].setText(str(c.conf['userId']['val']))

        k = 'devId2'
        self.ui.__dict__[k+'Label'].setText(c.conf['devId']['name'])
        self.ui.__dict__[k+'LineEdit'].setText(str(c.conf['devId']['val']))
        k = 'devId3'
        self.ui.__dict__[k+'Label'].setText(c.conf['devId']['name'])
        self.ui.__dict__[k+'LineEdit'].setText(str(c.conf['devId']['val']))
        k = 'devName2'
        self.ui.__dict__[k+'Label'].setText(c.conf['devName']['name'])
        self.ui.__dict__[k+'LineEdit'].setText(str(c.conf['devName']['val']))
        k = 'devName3'
        self.ui.__dict__[k+'Label'].setText(c.conf['devName']['name'])
        self.ui.__dict__[k+'LineEdit'].setText(str(c.conf['devName']['val']))
        k = 'calcPull2'
        self.ui.__dict__[k+'Label'].setText(c.conf['calcPull2']['name'])
        self.ui.__dict__[k+'LineEdit'].setText('{:.1f}'.format(c.conf['calcPull2']['val']).replace('.',','))
        k = 'calcPull3'
        self.ui.__dict__[k+'Label'].setText(c.conf['calcPull2']['name'])
        self.ui.__dict__[k+'LineEdit'].setText('{:.1f}'.format(c.conf['calcPull2']['val']).replace('.',','))
        k = 'training2'
        self.ui.__dict__[k+'Label'].setText(c.conf['training']['name'])
        self.ui.__dict__[k+'LineEdit'].setText(str(c.conf['training']['val']))
        k = 'podhod2'
        self.ui.__dict__[k+'Label'].setText(c.conf['podhod']['name'])
        self.ui.__dict__[k+'LineEdit'].setText(str(c.conf['podhod']['val']))
        k = 'level2'
        self.ui.__dict__[k+'Label'].setText(c.conf['level']['name'])
        self.ui.__dict__[k+'LineEdit'].setText(str(c.conf['level']['val']))
        k = 'weight2'
        self.ui.__dict__[k+'Label'].setText(c.conf['weight']['name'])
        self.ui.__dict__[k+'LineEdit'].setText(str(c.conf['weight']['val']))
        k = 'time2'
        self.ui.__dict__[k+'Label'].setText(c.conf['time']['name'])
        self.ui.__dict__[k+'LineEdit'].setText(str(c.conf['time']['val']))
        
    
    def __init__(self):
        super(window, self).__init__()
        self.ui = Ui_MainWindow()
        #self.ui.label_img.setPixmap(QPixmap('dev.jpg'))
        self.ui.setupUi(self)
        self.pswdTimer.timeout.connect(self._pswdTimeOut)
        
        
        for k in ['userId','userId2']: 
            self.ui.__dict__[k+'LineEdit'].setValidator( QIntValidator( 0, 9999) )
            self.ui.__dict__[k+'LineEdit'].setToolTip("0, 9999")
        for k in ['devId','devId2','devId3']: 
            self.ui.__dict__[k+'LineEdit'].setValidator( QIntValidator( 1, 20) )
            self.ui.__dict__[k+'LineEdit'].setToolTip("1, 20");

        for k in ['force1', 'force4', 'force7', 'force10']: 
            self.ui.__dict__[k+'LineEdit'].setValidator( QDoubleValidator( 10.00,110.00,2))
            self.ui.__dict__[k+'LineEdit'].setToolTip("10.00,110.00");

        """ Валидаторы """             
        for k in ['k1']: 
            self.ui.__dict__[k+'LineEdit'].setValidator( QDoubleValidator( 0.001,5.000,3) )
            self.ui.__dict__[k+'LineEdit'].setToolTip(" 0.001,5.000");
        for k in [ 'k2', 'k3', 'k4', 'k5', 'k6']: 
            self.ui.__dict__[k+'LineEdit'].setValidator( QDoubleValidator( 0.1,2.0,3) )
            self.ui.__dict__[k+'LineEdit'].setToolTip("0.1,2.0");
        for k in ['maxAngle']: 
            self.ui.__dict__[k+'LineEdit'].setValidator( QIntValidator( 10, 35) )
            self.ui.__dict__[k+'LineEdit'].setToolTip("10, 35");

        for k in ['kay']: 
            lkay= ['kayLineEdit_0' + str(i+1) for i in range(9)] + ['kayLineEdit_10']
            for i,lk in enumerate(lkay):
                self.ui.__dict__[lk].setValidator( QDoubleValidator( 0.0001,0.1,4) )
                self.ui.__dict__[lk].setToolTip(" 0.0001,0.1");
        for k in ['delta']: 
            self.ui.__dict__[k+'LineEdit'].setValidator( QDoubleValidator( 0.0,20.0,1))
            self.ui.__dict__[k+'LineEdit'].setToolTip("0.0,20.0");
        for k in ['training','podhod']: 
            self.ui.__dict__[k+'LineEdit'].setValidator( QIntValidator( 1, 10) )
            self.ui.__dict__[k+'LineEdit'].setToolTip("1, 10");
        for k in ['weight']: 
            self.ui.__dict__[k+'LineEdit'].setValidator( QIntValidator( 40, 140))
            self.ui.__dict__[k+'LineEdit'].setToolTip("40, 140");
        for k in ['pulse']: 
            self.ui.__dict__[k+'LineEdit'].setValidator( QIntValidator( 0, 220))
            self.ui.__dict__[k+'LineEdit'].setToolTip("0, 220");

            
       
            
        self.refreshWins()
        
        
        
        
    
    def keyPressEvent(self, event):
        """Обработка активации полей при вводе пароля "125"    """
        
#         if ( event.key() == Qt.__dict__['Key_'+str(conf['devId']['val'])]) and ( self.pswdState == 0):
#             self.pswdState = 1
#             self.pswdTimer.start(1000)
#             #print("1")  
        if ( event.key() == Qt.Key_1) and ( self.pswdState == 1):
            self.pswdState = 2
            self.pswdTimer.start(2000)
            self.pswd135 = False
            #print("2")  
        elif ( event.key() == Qt.Key_2 or  event.key() == Qt.Key_3 ) and ( self.pswdState == 2):
            self.pswdState = 3
            self.pswdTimer.start(2000)
            self.pswd135 = (event.key() == Qt.Key_3)
            #print("3")  
        elif ( event.key() == Qt.Key_5) and ( self.pswdState == 3):
            self.pswdState = 1
            self.pswdTimer.start(2000)
            print("pswd -OK")
            
            
            
            if self.pswd135:
                win.ui.plainTextEdit.setVisible(True)
                win.ui.checkBox_debug.setVisible(True)

                win.ui.fullScreenLabel.setVisible(True)
                win.ui.fullScreenLineEdit.setVisible(True)
                self.pswd135 = False
            else:
                 for le in self.les2: 
                        if le == 'kay':
                            for lk in self.lkay:
                                self.ui.__dict__[lk].setEnabled(True)
                        else:
                            self.ui.__dict__[le+'LineEdit'].setEnabled(True)
           
                        
        else:
            self.pswdState = 1
            self.pswd135 = False
        event.accept()
 


# In[14]:


win = window()


# ### Экран 1

# In[15]:


def pushButton_stop1(self):
    win.close()
win.ui.pushButton_stop1.clicked.connect(pushButton_stop1)


def  pushButton_newUser(self):
    win.ui.stackedWidget.setCurrentIndex(1)
    c.conf['training']['val']= 1
    c.conf['podhod']['val']= 1
    c.conf['userId']['val'] = 75
    c.conf['training']['val'] = 1
    c.conf['podhod']['val'] = 1
    c.conf['weight']['val'] = 75
    win.ui.label_V.setVisible(False)
    c.conf['time']['val'] = 300
    c.saveConf()
    win.refreshWins()
   
    win.refreshWins()
win.ui.pushButton_newUser.clicked.connect(pushButton_newUser)

def  pushButton_lastUser(self):
    win.ui.label_V.setVisible(True)
    win.ui.stackedWidget.setCurrentIndex(2)
    
    win.ui.userId2LineEdit.setFocus()
    win.ui.userId2LineEdit.selectAll()
win.ui.pushButton_lastUser.clicked.connect(pushButton_lastUser)


# ### Экран 2 - настройки

# In[16]:


def signal_sensorMeanVal(freq,val1,std1,val2,std2,err):
    if freq > 0 :
        win.ui.angleLineEdit.setText("ОК {} Гц. У1 = {:.1f} Гр  Погр1 = {:.1f}%  У2 = {:.1f} Гр  Погр2 = {:.1f}%   Ошб ={}".format(freq,val1,std1,val2,std2,err))
    else:
        win.ui.angleLineEdit.setText(c.conf['angle']['val'])
d.signal_sensorsStats.connect(signal_sensorMeanVal)

def signal_sensorDisconnected():
    win.ui.angleLineEdit.setText(c.conf['angle']['val'])

d.signal_sensorDisconnected.connect(signal_sensorDisconnected)

def pushButton_toStart(self):
    for le in win.les2: 
        if le == 'kay':
            for lk in win.lkay:
                win.ui.__dict__[lk].setEnabled(False)
        else:
            win.ui.__dict__[le+'LineEdit'].setEnabled(False)
    c.saveConf()
    win.refreshWins()
    win.ui.stackedWidget.setCurrentIndex(0)
win.ui.pushButton_toStart.clicked.connect(pushButton_toStart)

def pushButton_next(self):
    for le in win.les2: 
        if le == 'kay':
            for lk in win.lkay:
                win.ui.__dict__[lk].setEnabled(False)
        else:
            win.ui.__dict__[le+'LineEdit'].setEnabled(False)
    c.saveConf()
    win.refreshWins()
    win.ui.stackedWidget.setCurrentIndex(2)
    win.ui.userId2LineEdit.setFocus()
    win.ui.userId2LineEdit.selectAll()
win.ui.pushButton_next.clicked.connect(pushButton_next)    


signalMapper3 = QSignalMapper();   
for i, le in enumerate(win.inp):
    if le=='kay':
            for j,lk in enumerate(win.lkay):
                signalMapper3.setMapping(win.ui.__dict__[lk], 100+j)
                win.ui.__dict__[lk].editingFinished.connect(signalMapper3.map)
    else:
        signalMapper3.setMapping(win.ui.__dict__[le+'LineEdit'], i);
        win.ui.__dict__[le+'LineEdit'].editingFinished.connect(signalMapper3.map)

def les2editingFinished(i):
    if i >= 100:
        j = i -100
        le=win.lkay[j]                                
        c.conf['kay']['val'][j] = float(win.ui.__dict__[le].text().replace(',','.'))
    else:
        le = win.inp[i] 
        if le == 'devId': 
            c.conf[le]['val'] = int(win.ui.__dict__[le+'LineEdit'].text())
        elif le =='devName':
            c.conf[le]['val'] = win.ui.__dict__[le+'LineEdit'].text()
        elif le =='conn':
            c.conf[le]['val'] = win.ui.__dict__[le+'LineEdit'].text()
        elif le in  ['userId','userId2'] :
            c.conf['userId']['val'] = win.ui.__dict__[le+'LineEdit'].text()
        elif le in  ['force1', 'force4', 'force7', 'force10', 'maxAngle', 'training', 'podhod',  'weight','fullScreen'] :
            c.conf[le]['val'] = float(win.ui.__dict__[le+'LineEdit'].text().replace(',','.'))
        elif le in [ 'k1', 'k2', 'k3', 'k4', 'k5', 'k6','delta']:
            c.conf[le]['val'] = float(win.ui.__dict__[le+'LineEdit'].text().replace(',','.'))
        elif le in ['delta']:
            c.conf[le]['val'] = float(win.ui.__dict__[le+'LineEdit'].text().replace(',','.'))
            l._posDeltaAngle = c.conf[le]['val']
        elif le == 'f1f2': 
            c.conf[le]['val'] = '+' if win.ui.__dict__[le+'LineEdit'].text() == '+' else '-'
        else:
            print('ERROR123')
    c.saveConf()
    win.refreshWins()



signalMapper3.mapped.connect(les2editingFinished)


# ### Экран 3 ввод данных

# In[17]:


def pushButton_toStart2(self):
    win.ui.stackedWidget.setCurrentIndex(0)
    win.ui.pushButtonStart0.setFocus()
win.ui.pushButton_toStart2.clicked.connect(pushButton_toStart2)



def pushButton_run(self):
    win.ui.label_V.setVisible(False)
    c.saveConf()
    l.start()
    win.ui.dt3Label.setText('Дата')
    win.ui.dt3LineEdit.setText(strftime("%d.%m.%Y", localtime()))
    c.conf['dt2']['val'] = win.ui.dt3LineEdit.text()
    win.ui.pushButton_pause.setVisible(True)
    win.ui.pushButton_stop2.setVisible(True)
    win.ui.pushButton_resume.setVisible(False)
    win.ui.pushButton_end.setVisible(False)
    win.ui.pushButton_save.setVisible(False)
    win.ui.pushButton_toStart3.setVisible(False)
    win.ui.pushButton_pulse.setVisible(False)
    win.ui.label_testEnd.setVisible(False)
    win.ui.pulseLineEdit.setVisible(False)
    win.ui.pulseLabel.setVisible(False)

    win.ui.plainTextEdit.setVisible(False)
    win.ui.checkBox_debug.setVisible(False)
    win.ui.fullScreenLabel.setVisible(False)
    win.ui.fullScreenLineEdit.setVisible(False)
    win.ui.stackedWidget.setCurrentIndex(3)
    win.ui.pushButton_pause.setFocus()
    
    win.ui.label_time.setText(     '{:d}  '.format(int(0)))
    win.ui.label_move.setText(     '{:d}  '.format(int(0)))
 
    win.ui.label_work.setText( '{:.1f}  '.format(0).replace('.',','))
    win.ui.label_kDj.setText( '    Дж')
    win.ui.label_power.setText(    '{:.1f}  '.format(0).replace('.',','))
    win.ui.label_powerMean.setText('{:.1f}  '.format(0).replace('.',','))
    
    win.refreshWins()
win.ui.pushButton_run.clicked.connect(pushButton_run) 
win.ui.pushButtonStart0.clicked.connect(pushButton_run)

lbuts= ['pushButton_l0' + str(i+1) for i in range(9)] + ['pushButton_l10']
signalMapper = QSignalMapper()
for i,butName in enumerate(lbuts):
    signalMapper.setMapping(win.ui.__dict__[butName], i+1);
    win.ui.__dict__[butName].clicked.connect(signalMapper.map)


mbuts = [['pushButton_m0' + str(i),i] for i in [1,2,3,4,5]] + [['pushButton_m' + str(i),i] for i in [10,15,20,25,30]] 
signalMapper2 = QSignalMapper()

for butName,m in mbuts:
    signalMapper2.setMapping(win.ui.__dict__[butName], m*60);
    win.ui.__dict__[butName].clicked.connect(signalMapper2.map)


def pushButton_level(i):
    win.ui.levelLineEdit.setText(str(i))
    c.conf['level']['val'] = i
    c.saveConf()
    win.refreshWins()

def pushButton_time(i):
    win.ui.timeLineEdit.setText(str(i))
    c.conf['time']['val'] = i
    c.saveConf()
    win.refreshWins()
    

signalMapper.mapped.connect(pushButton_level)
signalMapper2.mapped.connect(pushButton_time)


# ### Экран 4 - выполнение упражнения

# In[18]:


def refreshLabels(logicRes):
    time = logicRes[0]
    #Время	         130   сек
    #Движений           41
    #Работа	           1,3  кДж
    #Мощность           51   Вт
    win.ui.label_time.setText(     '{:d}  '.format(int(time)))
    win.ui.plainTextEdit.setPlainText("""Угол {:.2f} = {:.2f} - {:.2f}  + {:.2f} - {:.2f}
дельта от старта {:.2f} = {:.2f} - {:.2f}
f1 {:.2f}
f2 {:.2f}
_deltaPos {:.2f}
_amp {:.2f}
work {:.2f}""".format(l._val,l._valf1,l._posStart1, l._valf2, l._posStart2, l._posCur - l._posStart,l._posCur ,l._posStart, c.f1, c.f2, l._deltaPos, l._amp,  l.work))
    
    win.update()
def refreshLabels2(logicRes):
    time, move, work, power, powerMean = logicRes
    #Время	         130   сек
    #Движений           41
    #Работа	           1,3  кДж
    #Мощность           51   Вт
    win.ui.label_time.setText(     '{:d}  '.format(int(time)))
    win.ui.label_move.setText(     '{:d}  '.format(int(move)))
    if work >= 1000:
        win.ui.label_work.setText( '{:.1f}  '.format(work/1000.0).replace('.',','))
        win.ui.label_kDj.setText( '    кДж')
    else:
        win.ui.label_work.setText( '{:.1f}  '.format(work).replace('.',','))
        win.ui.label_kDj.setText( '    Дж')
    win.ui.label_power.setText(    '{:.1f}  '.format(power).replace('.',','))
    win.ui.label_powerMean.setText('{:.1f}  '.format(powerMean).replace('.',','))
    
    win.update()
l.signalCalcRes.connect(refreshLabels)
l.signalMoveRes.connect(refreshLabels2)
win.ui.checkBox_debug.stateChanged.connect(l.slotDebugStateChanged)


def pushButton_pause(self):
    l.pause()

    win.ui.pushButton_pause.setVisible(False)
    win.ui.pushButton_stop2.setVisible(False)
    win.ui.pushButton_resume.setVisible(True)
    win.ui.pushButton_end.setVisible(True)
    win.ui.pushButton_save.setVisible(False)
    win.ui.pushButton_toStart3.setVisible(False)
    win.ui.pushButton_pulse.setVisible(False)
    win.ui.label_testEnd.setVisible(False)
    win.ui.pulseLineEdit.setVisible(False)
    win.ui.pulseLabel.setVisible(False)

    win.ui.pushButton_resume.setFocus()
    
win.ui.pushButton_pause.clicked.connect(pushButton_pause)
win.ui.pushButton_stop2.clicked.connect(pushButton_pause)


def pushButton_stop2(self):
    l.stop()
    win.ui.pushButton_pause.setVisible(False)
    win.ui.pushButton_stop2.setVisible(False)
    win.ui.pushButton_resume.setVisible(False)
    win.ui.pushButton_end.setVisible(False)
    win.ui.pushButton_save.setVisible(True)
    win.ui.pushButton_toStart3.setVisible(True)
    win.ui.pushButton_pulse.setVisible(False)
    win.ui.label_testEnd.setVisible(True)
    win.ui.pulseLineEdit.setVisible(False)
    win.ui.pulseLabel.setVisible(False)

    win.ui.pushButton_save.setFocus()

win.ui.pushButton_end.clicked.connect(pushButton_stop2)
l.signalTestEnd.connect(pushButton_stop2)


def pushButton_resume(self):
    l.resume()
    win.ui.pushButton_pause.setVisible(True)
    win.ui.pushButton_stop2.setVisible(True)
    win.ui.pushButton_resume.setVisible(False)
    win.ui.pushButton_end.setVisible(False)
    win.ui.pushButton_save.setVisible(False)
    win.ui.pushButton_toStart3.setVisible(False)
    win.ui.pushButton_pulse.setVisible(False)
    win.ui.label_testEnd.setVisible(False)
    win.ui.pulseLineEdit.setVisible(False)
    win.ui.pulseLabel.setVisible(False)

    win.ui.pushButton_pause.setFocus()
win.ui.pushButton_resume.clicked.connect(pushButton_resume)

win.ui.pushButton_toStart3.clicked.connect(pushButton_toStart2)


def pushButton_save(self):
    win.ui.pushButton_pause.setVisible(False)
    win.ui.pushButton_stop2.setVisible(False)
    win.ui.pushButton_resume.setVisible(False)
    win.ui.pushButton_end.setVisible(False)
    win.ui.pushButton_save.setVisible(False)
    win.ui.pushButton_toStart3.setVisible(False)
    win.ui.pushButton_pulse.setVisible(True)
    win.ui.label_testEnd.setVisible(False)
    win.ui.pulseLineEdit.setVisible(True)
    win.ui.pulseLabel.setVisible(True)

    win.ui.pulseLineEdit.setFocus()
    win.ui.pulseLineEdit.selectAll()
    
win.ui.pushButton_save.clicked.connect(pushButton_save)


# def pulseLineEdittextChanged():
#     win.ui.pushButton_pulse.setEnabled(False)
# win.ui.pulseLineEdit.textChanged.connect(pulseLineEdittextChanged)



# def pulseLineEditFinish():
#     win.ui.pushButton_pulse.setEnabled(True)
# win.ui.pulseLineEdit.editingFinished.connect(pulseLineEditFinish)



def pushButton_pulse(self):
       
    pulse = int(win.ui.pulseLineEdit.text())
    if pulse !=0 and  (pulse <40 or pulse >220): 
        win.ui.pulseLineEdit.setText("0")
        win.ui.pulseLineEdit.setFocus()
        win.ui.pulseLineEdit.selectAll()
        return
    
    saveResToXlsx('файл тестов1.xlsx')
    saveResToXlsx('файл тестов2.xlsx')
    saveResToXlsx('файл тестов3.xlsx')
        

    
    if l.debugState == 2:
        file = open("debug1.csv", "w")
        file.write( "стартВремя;"+ ";".join([ "{:.3f}".format(x) for x in l._startt ]).replace(".",",") + "\n")
        file.write( "стoпВремя;"+ ";".join([ "{:.3f}".format(x) for x in l._stopt ]).replace(".",",") + "\n")
        file.write( "пауза;номер;время;угол1;угол2;положение\n")
        for j in range(len(l._lvals)):
            for i in range(len(l._lvals[j])):
                file.write("{};{};".format(j,i) + ";".join(["{:.2f}".format(x) for x in [l._lvt[j][i],l._lvals1[j][i],l._lvals2[j][i], l._lvals[j][i] ] ]).replace(".",",")+"\n" )
        file.close()  

    if l.debugState == 2:
        
        moveCnt,pwr,fullwork, minmaxs, debugline, head = l.find_peaksWork()
        file = open("debug2.csv", "w")
        file.write(head+"\n")
        for Line in debugline:
              file.write(str(Line).replace(",",";").replace(".",",").replace("]","\n").replace("[",""))
        file.close()  
        
        file = open("debug3.csv", "w")
        file.write("minmaxs")
        for Line in minmaxs:
              file.write(str(Line).replace(",",";").replace(".",",").replace("[","") + "\n")
        file.close()  
    
    pushButton_toStart2(self)
win.ui.pushButton_pulse.clicked.connect(pushButton_pulse)


# ## Cохранение результатов теста

# In[19]:


import openpyxl

def saveResToXlsx(xlsxFileName):
    """Cохранение результатов теста в xlsx"""
    
    moveCnt,pwr,fullwork,_,_,_ = l.find_peaksWork()
    head = ['Дата',
            'Время',
            'Личный №',
            'Тренировка',
            'Тренажер №',
            'Наименование',
            'Подход №',
            '№ на рычаге',
            'Значение усилия, %',
            'Масса человека, кг',
            'Время теста, сек',
            'Число движений',
            'Работа общая, Дж',
            'Мощность средняя, Вт',
            'Пульс в конце',
            'Др. параметр']
    pulse = int(win.ui.pulseLineEdit.text())
    res =  [  strftime("%d.%m.%Y", localtime()),
            strftime("%H:%M:%S", localtime()),
     c.conf['userId']['val'],
     c.conf['training']['val'],
     c.conf['devId']['val'],
     c.conf['devName']['val'],
     c.conf['podhod']['val'],
     c.conf['level']['val'],
     round(c.conf['calcPull2']['val'],1),
     c.conf['weight']['val'],
     round(l.time,1),
     moveCnt,#l.move,
     round(fullwork,1), #round(l.work,1),
     round(fullwork/l.time if l.time >0 else 0.0,1),#round(l.powerMean(),1),
     pulse,
     #"\t".join( [("{:.2f}".format(x)).replace(".",",") for x in l._vals] )
     ""      ]

    try :
        ws = openpyxl.load_workbook(xlsxFileName)
    except:
        ws = openpyxl.Workbook()
        sh = ws.worksheets[0]
        for i,h in enumerate(head):
            sh.cell(1,i+1).value = h
        try :
            ws.save(xlsxFileName)
        except:
            print('ERR ws.save(xlsxFileName)')
    ws = openpyxl.load_workbook(xlsxFileName)
    sh = ws.worksheets[0]
    row= sh.max_row+1

    for i,r in enumerate(res):
        sh.cell(row,i+1).value = r
    try :
        ws.save(xlsxFileName)
    except:
        print('ERR ws.save(xlsxFileName)')



# ## Запуск приложения

# In[20]:


win.ui.stackedWidget.setCurrentIndex(0)
win.ui.plainTextEdit.setVisible(False)
win.ui.checkBox_debug.setVisible(False)

win.ui.fullScreenLabel.setVisible(False)
win.ui.fullScreenLineEdit.setVisible(False)
win.ui.pushButtonStart0.setFocus()

c.conf['dt']['val'] = strftime("%d.%m.%Y", localtime())
c.conf['userId']['val'] = 75
c.conf['training']['val'] = 1
c.conf['podhod']['val'] = 1
c.conf['level']['val'] = 4
c.conf['weight']['val'] = 75
c.conf['time']['val'] = 300
c.saveConf()
win.refreshWins()



if c.conf['fullScreen']['val'] == 1:
    win.setWindowFlags(Qt.WindowStaysOnTopHint | Qt.FramelessWindowHint);
    win.showMaximized()
else:
    win.showMaximized()

app.exec()

