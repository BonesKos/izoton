#!/usr/bin/env python
# coding: utf-8

# In[1]:


from PyQt5 import QtWidgets, uic
from PyQt5.QtGui import QIcon, QPixmap,QIntValidator,QIntValidator 
from PyQt5.QtCore import Qt, pyqtSignal, QObject, QTimer,QByteArray
from PyQt5 import  QtNetwork
import sys, random


# In[2]:


app = QtWidgets.QApplication([])


# In[3]:


from imitator_ui import Ui_MainWindow  # импорт сгенерированного файла из ui

class window(QtWidgets.QMainWindow):
     def __init__(self):
        super(window, self).__init__()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

win = window()


# In[4]:


#win = uic.loadUi("imitator.ui")

win.ui.portLineEdit.setValidator( QIntValidator( 1, 65535) )
win.ui.delayLineEdit.setValidator( QIntValidator( 1, 1000) )
win.ui.dataDelayLineEdit.setValidator( QIntValidator( 50, 2000) )
#win.ui.valLineEdit.setValidator( QIntValidator( -180, 180) )


# In[5]:


win.ui.radioButton_sendObj.setAutoExclusive(False)
win.ui.radioButton_Client.setAutoExclusive(False)


# In[6]:


tcpServer = QtNetwork.QTcpServer()
port = int(win.ui.portLineEdit.text())
tcpServer.listen(QtNetwork.QHostAddress.Any, port)


# In[7]:


def editingPortFinished():
    tcpServer.close()
    port = int(win.ui.portLineEdit.text())
    tcpServer.listen(QtNetwork.QHostAddress.Any, port)
    print("New port is", port)
    
win.ui.portLineEdit.editingFinished.connect(editingPortFinished)

def delayLineEdit():
    timer.stop()
    timer.start(int(win.ui.delayLineEdit.text()))
    print(int(win.ui.delayLineEdit.text()))
win.ui.delayLineEdit.editingFinished.connect(delayLineEdit)

def editingvalLineEditFinished():
    win.ui.label_val.setText(win.ui.valLineEdit.text())
win.ui.valLineEdit.editingFinished.connect(editingvalLineEditFinished)


# In[8]:


timer = QTimer()
timer.start(int(win.ui.delayLineEdit.text()))
socket = QtNetwork.QTcpSocket()


# In[9]:


timerDataDelay = QTimer()
timerDataCnt = 0
data = ""


# In[10]:


def on_timerDataDelay():
    global timerDataCnt
    
    if timerDataCnt < len(data) :
        win.ui.label_val.setText(data[timerDataCnt])
        win.ui.label.setText("Серия значений [{}] + '\\n'".format(timerDataCnt))
        timerDataCnt += 1
    elif win.ui.checkBox.checkState() == 0:
        timerDataCnt = 0
        timerDataDelay.stop()
    else:
        timerDataCnt = 0
        


# In[ ]:





# In[11]:


timerDataDelay.timeout.connect(on_timerDataDelay)


# In[12]:


def pushButton():
    global timerDataCnt
    global data
    data = win.ui.plainTextEdit1.toPlainText().replace('\r','').split('\n')
    timerDataCnt = 0 #len(data)
    timerDataDelay.stop()
    timerDataDelay.start(int(win.ui.dataDelayLineEdit.text()))
    
win.ui.pushButton.clicked.connect(pushButton)

def checkBoxstateChanged(state):
    if state == 0:
        timerDataDelay.stop()

win.ui.checkBox.stateChanged.connect(checkBoxstateChanged)


# In[13]:


def timeout():
    global socket
    global val
    global timer
    
    if socket.state() == 3:
        win.ui.radioButton_sendObj.setChecked(not win.ui.radioButton_sendObj.isChecked())
        win.update()
#         try:
#             val = int(win.ui.label_val.text())
#         except:
#             val = 0
#         val += round(random.random())

        val = win.ui.label_val.text()
        val = "{:.2f},{:.2f}".format(float(val.split(',')[0]) + (random.random()-0.5)*win.ui.DoubleSpinBox.value(),float(val.split(',')[1]) + (random.random()-0.5)*win.ui.DoubleSpinBox.value())

        socket.write(QByteArray( (str(val)+"\r\n").encode() ));
        socket.flush();
        socket.waitForBytesWritten(50);
        #socket->close(); 
        #timer.start(int(win.ui.delayLineEdit.text()))
        
    else:
        #timer.stop()
        win.ui.radioButton_Client.setChecked(False)
        win.update()


# In[14]:


timer.timeout.connect(timeout)

def newuser():
    global socket
    global timerDataCnt
    timerDataCnt = 0
    if socket.state() == 3: socket.close()
    socket = tcpServer.nextPendingConnection();
    socket.setSocketOption(QtNetwork.QAbstractSocket.KeepAliveOption, 1)
    socket.setSocketOption(QtNetwork.QAbstractSocket.LowDelayOption, 1)
    win.ui.radioButton_Client.setChecked(True)
    win.update()
    timer.start(int(win.ui.delayLineEdit.text()))


# In[15]:


tcpServer.newConnection.connect(newuser)


# In[ ]:


win.show()
win.ui.pushButton.setFocus()
app.exec()

